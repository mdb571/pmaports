# Maintainer: Duje Mihanović <duje.mihanovic@skole.hr>

_flavor=samsung-coreprimevelte
pkgname=linux-$_flavor
pkgver=6.7_rc6
pkgrel=0
pkgdesc="Samsung Galaxy Core Prime VE LTE mainline kernel"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/LegoLivesMatter/linux"
_commit="fcbf65a4f68cab65de26bc95613785b93996db22"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-nftables
pmb:kconfigcheck-zram"
makedepends="bash bc bison devicepkg-dev findutils flex gmp-dev mpc1-dev
mpfr-dev openssl-dev perl postmarketos-installkernel pxa-mkbootimg dtc"

# Source
_config="config-$_flavor.$arch"
source="
	$pkgname-$_commit.tar.gz::$url/-/archive/$_commit/linux-$_commit.tar.gz
	$_config
"
builddir="$srcdir/linux-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" "$builddir"/.config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

	install -D include/config/kernel.release \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	# Master DTB (deviceinfo_bootimg_qcdt)
	pxa1908-dtbTool -p scripts/dtc/ -o arch/$_carch/boot/dt.img arch/$_carch/boot/dts/marvell/
	install -Dm644 arch/$_carch/boot/dt.img "$pkgdir"/boot/dt.img
}

sha512sums="
5bd87f6b9582cebae24316b9ee1a62e837aaa6d342a2833b35ba6fda83f8e0519a673a8879cff171eaef76c52948ee19a4286b2badb33412b6b312be7ff67a15  linux-samsung-coreprimevelte-fcbf65a4f68cab65de26bc95613785b93996db22.tar.gz
caa4d561713c6b1df96774aa77bae713fdc9b7a2b87272fd37b55ef1046b932fc7a9edb3b2045562741101f8b785e50e004aba01c6f26c7a375722184f2ab018  config-samsung-coreprimevelte.aarch64
"
